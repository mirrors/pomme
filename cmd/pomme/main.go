package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "dns.froth.zone/pomme/docs"
	"dns.froth.zone/pomme/frontend"
	"dns.froth.zone/pomme/internal"
	"dns.froth.zone/pomme/internal/api"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
)

// @title						Pomme
// @version					0.0.1
// @description				Pomme is a service that parses zonefiles
// @termsOfService				http://freecumextremist.com/
//
// @securityDefinitions.apikey	Bearer
// @in							header
// @name						Authorization
// @description				Type "Bearer" followed by a space and JWT token.
func main() {
	config, err := internal.ReadConfig()
	if err != nil {
		panic(err)
	}

	pomme := chi.NewRouter()
	pomme.Use(middleware.Logger)
	pomme.Use(middleware.GetHead)
	pomme.Use(middleware.Recoverer)

	pomme.Mount("/", frontend.SvelteKitHandler("/"))
	pomme.Mount("/api", api.API())
	pomme.Mount("/swagger", httpSwagger.WrapHandler)

	log.Println("\t-------------------------------------")
	log.Println("\t\tRunning on port " + config.Port)
	log.Println("\t-------------------------------------")

	s := &http.Server{
		ReadTimeout:  3 * time.Second,
		WriteTimeout: 15 * time.Second,
		Addr:         ":" + config.Port,
		Handler:      pomme,
	}

	idleConnsClosed := make(chan struct{})

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		if err := s.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}

		close(idleConnsClosed)
	}()

	if err := s.ListenAndServe(); err != http.ErrServerClosed { // nolint: goerr113
		// Error starting or closing listener:
		log.Fatalf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}
