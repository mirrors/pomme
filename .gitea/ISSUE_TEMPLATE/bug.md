---
name: Bug report
about: Report a bug
---

**Describe the bug**

A clear and concise description of what the bug is.

**Reproduction steps**

Steps to reproduce the behavior:

1. 
2. 
3. Bug

**Expected behaviour**

A clear and concise description of what you expected to happen.

**Screenshots / Logs**

```
Add any backend logs or browser console output related to your issue: 
```

```
Add any screenshots related to a frontend issue
```

**System information (please complete the following information):**

- OS: [e.g. Ubuntu 22.04, OpenBSD, Windows 11]
- Pomme Version

**Additional context**

Add any other context about the problem here.