package internal

import "golang.org/x/sys/unix"

func SysKill() (err error) {
	err = killPomme()

	return
}

func killPomme() (err error) {
	err = unix.Kill(unix.Getpid(), unix.SIGINT)

	return
}
