package internal

import "gorm.io/gorm"

// User struct represents a user in the database.
type User struct {
	gorm.Model
	Username       string
	HashedPassword string
}

// Response struct represents a json response.
type Response struct {
	Message string `json:"message,omitempty"`
}

// ZoneRequest represents a Zone file request.
type ZoneRequest struct {
	*Zone

	User string `json:"user,omitempty" gorm:"foreignKey:username;references:User"`
}

// Zone struct represents a zonefile in the database.
type Zone struct {
	gorm.Model
	FileName string `json:"name"`
	Body     string `json:"body"`
}

type Config struct {
	Server        string
	HashingSecret string
	Port          string
	DB            string
	TestDB        string
	ZoneDir       string
}

// SwaggerGenericResponse[T]
// @Description Generic Response for Swagger.
type SwaggerGenericResponse[T any] struct {
	// Response items
	Response T
}
