package internal

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	testCases := []struct {
		name         string
		fileContents string
	}{
		{
			name: "Should fail to read config",
		},
		{
			name: "Should read config file successfully",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.name == "Should read config file successfully" {
				f, err := os.Create("config.yaml")
				if err != nil {
					assert.NotNil(t, err)
				}
				defer os.Remove(f.Name()) //nolint: errcheck
			} else {
				err := os.Remove("config.yaml")
				if err != nil {
					assert.NotNil(t, err)
				}
			}

			config, err := ReadConfig()
			if err != nil {
				assert.NotNil(t, err)
			}

			switch tc.name {
			case "Should read config file successfully":
				assert.NotNil(t, config)
			default:
				assert.Nil(t, config)
			}
		})
	}
}
