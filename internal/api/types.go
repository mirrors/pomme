package api

import (
	"net/http"

	"gorm.io/gorm"
)

const (
	keyPrincipalContextID key = iota
)

type key int

// ZoneRequest represents a new zone file request.
//
// Inside it is a pointer to the zone struct, which contains zone file information, and a User field to keep track of whom owns the file/request.
type ZoneRequest struct {
	*Zone

	User string `json:"user,omitempty" gorm:"foreignKey:username;references:User"`
}

// Zone struct represents a zone file.
type Zone struct {
	gorm.Model

	// FileName is the file name for an uploaded zone file how it is expected to show up on the filesystem
	FileName string `json:"name"`

	// Body is the bytes array of a zone files body for copying and moving it around
	Body []byte `json:"body,omitempty"`
}

// ndr is an interface for new DNS requests. It's methods can be used with a ZoneRequest object.
type ndr interface {
	// parse() is a wrapper around miekg's NewZoneParser, which is used to validate uploaded zone files
	//
	// if no error is raised the zone file can be saved.
	parse() error

	// save() is a wrapper around internal.makeLocal() which will save a valid non-empty zone file to the filesystem
	//
	// the file is saved to a location the user sets up in their config.yaml file,
	// once saved it can be consumed by something like NSD.
	save() error
}

var _ ndr = (*ZoneRequest)(nil)

// HTTPResponder interface exists for type constraints on generic HTTP logging functions.
type HTTPResponder interface {
	any
}

// HTTPLogger interface handles logging and sending responses to the user.
type HTTPLogger[T HTTPResponder] interface {
	apiError(http.ResponseWriter, *http.Request)
	writeLogEntry()
}

// Response is a generic response struct containing the necessary keys for a log and response action.
type Response[T HTTPResponder] struct {
	Message, Status, Realm, Err T
}

var _ HTTPLogger[HTTPResponder] = (*Response[HTTPResponder])(nil)

// newResponder instantiates a new HTTPLogger object.
func newResponder[T HTTPResponder](m Response[T]) HTTPLogger[T] {
	return &Response[T]{
		Message: m.Message,
		Status:  m.Status,
		Realm:   m.Realm,
		Err:     m.Err,
	}
}
