package api

import (
	"fmt"
	"time"

	"dns.froth.zone/pomme/internal"
	"github.com/go-chi/jwtauth/v5"
)

var tokenAuth *jwtauth.JWTAuth

func init() {
	if config, err := internal.ReadConfig(); err == nil {
		tokenAuth = jwtauth.New("HS256", []byte(config.HashingSecret), nil)
	}
}

func makeToken(username string) (tokenString string, err error) {
	claim := map[string]interface{}{"username": username, "admin": false}

	jwtauth.SetExpiry(claim, time.Now().Add(time.Hour))

	if _, tokenString, err = tokenAuth.Encode(claim); err == nil {
		return
	}

	return "", fmt.Errorf("unable to generate JWT: %w", err)
}
