package db

import (
	"fmt"

	"dns.froth.zone/pomme/internal"
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
)

// InitDb is the init function for the database.
func InitDb(path, mode string) (db *gorm.DB, err error, ok bool) {
	ok = true

	if mode == "test" {
		path = "pomme-test.sqlite"
	}

	db, err = gorm.Open(sqlite.Open(path), &gorm.Config{})

	if err != nil {
		return db, fmt.Errorf("failed to connect database: %w", err), !ok
	}

	// Migrate the schema
	err = db.AutoMigrate(&internal.User{}, &internal.ZoneRequest{})
	if err != nil {
		return &gorm.DB{}, fmt.Errorf("failed to run DB migration: %w", err), !ok
	}

	return db, nil, ok
}
