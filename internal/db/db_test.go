package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDB(t *testing.T) {
	testCases := []struct {
		name   string
		dbMode string
		dbName string
	}{
		{
			name:   "Should fail to open db",
			dbMode: "nothing",
			dbName: "",
		},
		{
			name:   "Should open test db",
			dbMode: "test",
			dbName: "",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err, ok := InitDb(tc.dbName, tc.dbMode)
			if err != nil && !ok {
				assert.NotNil(t, err)
			}
		})
	}
}
