package internal

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/adrg/xdg"
	"gopkg.in/yaml.v3"
)

var config Config

func ReadConfig() (*Config, error) {
	var (
		data              []byte
		err               error
		defaultConfigPath string
	)

	defaultConfigPath = xdg.ConfigHome + "/pomme/config.yaml"

	if data, err = os.ReadFile(filepath.Clean(defaultConfigPath)); err == nil {
		if err = yaml.Unmarshal(data, &config); err != nil {
			return nil, fmt.Errorf("unable to unmarshal config file: %w", err)
		}

		return &config, nil
	}

	if data, err = os.ReadFile(filepath.Clean("./config.yaml")); err == nil {
		if err = yaml.Unmarshal(data, &config); err != nil {
			return nil, fmt.Errorf("unable to unmarshal config file: %w", err)
		}

		return &config, nil
	}

	return nil, fmt.Errorf("unable to read config file: %w", err)
}
