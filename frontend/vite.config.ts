import { optimizeCss } from 'carbon-preprocess-svelte';
import { sveltekit } from '@sveltejs/kit/vite';

const config: import('vite').UserConfig = {
	plugins: [sveltekit(), process.env.NODE_ENV === 'production' && optimizeCss()],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	},
	server: {
		proxy: {
			'/api': 'http://localhost:3008',
			'/swagger': 'http://localhost:3008'
		}
	},
	build: {
		sourcemap: true
	}
};

export default config;
