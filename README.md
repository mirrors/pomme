[![Build Status](https://ci.freecumextremist.finance/api/badges/grumbulon/pomme/status.svg)](https://ci.freecumextremist.finance/grumbulon/pomme)

This will be a website that, to oversimplify things, allows users to submit zonefiles to use [froth.zone nameservers](https://dns.froth.zone/nameservers).

More information to come soon.

## Running
To build & install you need both Go and Node (>= 18 should work)

You also need to [install pnpm](https://pnpm.io/installation).

to generate and build:

```bash
go generate ./...
go build ./cmd/pomme
```

## Testing
To test pomme you can run

```bash
make test-backend
```
