CGO_ENABLED ?= 0
GOFLAGS ?= -trimpath -ldflags="-s -w"

PROG ?= pomme
FRONTEND ?= frontend/build/index.html
GO_SOURCES ?= $(shell find . -name "*.go" -type f ! -name '*_test*')
GO_TEST_SOURCES ?= $(shell find . -name "*_test.go" -type f)

NODE_SOURCES ?= $(shell find ./frontend -type d \( -name node_modules -o -name .svelte-kit -o -name tests -o -name build \) -prune -o \( -name '*.ts' -o -name '*.js' -o -name '*.html' -o -name '*.svelte' -o -name '*.json' \) -print)

all: frontend backend

.PHONY: frontend
$(FRONTEND): $(NODE_SOURCES)
	pnpm -F frontend build
frontend: $(FRONTEND)

.PHONY: backend
$(PROG): $(GO_SOURCES) $(NODE_SOURCES)
	go build $(GOFLAGS) ./cmd/pomme
backend: $(PROG)

.PHONY: format format-frontend format-backend
format: format-frontend format-backend

format-frontend:
	pnpm -F frontend format

format-backend:
	gofmt -w -s .

swagger:
	swag init -d cmd/pomme/,internal/

.PHONY: lint lint-frontend lint-backend
lint: lint-frontend lint-backend

lint-frontend:
	pnpm -F frontend lint

lint-backend:
	golangci-lint run --timeout=5m --out-format colored-line-number:stdout

.PHONY: test test-frontend test-backend
test: test-frontend test-backend

test-frontend:
	pnpm -F frontend "test:unit"

test-backend:
	go test -v -cover ./internal/...
